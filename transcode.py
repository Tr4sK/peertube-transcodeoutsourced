#!/usr/bin/python3
import requests
import json
import pprint
import subprocess
import pipes
import paramiko
import os
import ffprobe3
import argparse
import configparser
import sys
import mimetypes
from requests_toolbelt.multipart.encoder import MultipartEncoder
from requests_oauthlib import OAuth2Session
from oauthlib.oauth2 import LegacyApplicationClient
import logging
from unidecode import unidecode
import youtube_dl

transcode_format = [ '1080', '720', '480', '360', '240' ]
transcode_fps_max = 60
transcode_fps_average = 30
transcode_fps_min = 10
transcode_threads = 6
transcode_audio_baseKbitrate = 384
remote_path="/var/www/peertube/storage/videos/"
local_path="peertube/"

log_level = 15

logging.basicConfig(
    level=int(log_level),
    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-7.7s] %(message)s",
    handlers=[
        logging.StreamHandler()
    ]
)
def verbose(msg, *args, **kwargs):
    if logging.getLogger().isEnabledFor(15):
        logging.log(15, msg)
logging.addLevelName(15, "VERBOSE")
logging.verbose = verbose
logging.Logger.verbose = verbose

def args():
    ActionHelp = """
        user : Only transcode videos from that user (-u is required)
        channel : Only transcode videos from that channel (-c is required)
        videos: Start transcoding the last 100 local video posted. (Default)
        import: Transcode a local video from any format and import it.
        """
    parser = argparse.ArgumentParser(prog="PeerTube Transcode Outsourced", description="Transcode video in lower rez from another computer/server.", formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('action', nargs='?', choices=('user', 'channel', 'videos', 'import'), default='videos', help=ActionHelp)
    parser.add_argument("-a", "--api", help="Define peertube API version (default 'v1')")
    parser.add_argument("-s", "--server", help="Define SSH server (tube.tr4sk.me)")
    parser.add_argument("-l", "--login", help="Define user account")
    parser.add_argument("-P", "--password", help="Define user password")
    parser.add_argument("-f", "--config-file", help="Define a config file (.ini)")
    parser.add_argument("-c", "--channel-id", help="Define channel id")
    parser.add_argument("-u", "--user-id", help="Define target user account")
    parser.add_argument("-k", "--ssh-key", help="Specify a ssh_key to use")
    parser.add_argument("-V", "--video-file", help="Define video to import, for import mode")
    parser.add_argument("-n", "--video-name", help="Define video name")
    parser.add_argument("-U", "--url", help="Define url to import (youtube-dl), for import mode")
    parser.add_argument("-t", "--thumbnail", help="Add thumbnail to the video")
    parser.add_argument("-d", "--do-not-transcode", help="Do not transcode, for import mode)", action="store_true")
    parser.add_argument("-v", "--verbose", help="Increase verbosity for debugging", action="store_true")
    args = parser.parse_args()
    return args

def cleanString(toclean):
    toclean = toclean.decode('utf-8')
    toclean = unidecode(toclean)
    cleaned = re.sub('[^A-Za-z0-9]+', '', toclean)
    return cleaned

def get_oauth_token(session, api, username, password):
    url = str(api).rstrip("/") + "/oauth-clients/local"
    
    try:
        response = session.get(url)
        client_id = response.json()['client_id']
        client_secret = response.json()['client_secret']
    except Exception as e:
        if hasattr(e, 'message'):
            logging.error('Get OAUTH Token failed: %s' % str(e.message))
        else:
            logging.error('Get OAUTH Token failed: %s' % str(e))


    url = str(api).rstrip("/")
    oauth_client = LegacyApplicationClient(client_id=str(client_id))
    try:
        oauth = OAuth2Session(client=oauth_client)
        oauth.fetch_token(
            token_url=str(url + '/users/token'),
            # lower as peertube does not store uppercase for pseudo
            username=str(username).lower(),
            password=str(password),
            client_id=str(client_id),
            client_secret=str(client_secret)
        )
    except Exception as e:
        if hasattr(e, 'message'):
            logging.error("Error while fetching token: " + str(e.message))
            exit(1)
        else:
            logging.error("Error while fetch token: " + str(e))
            exit(1)
    return oauth


def get_videos(api, oauth, filter='local', count='50'):
    url = str(api).rstrip('/') + "/videos"
    params = { 'filter': filter, 'count': count}
    try:
        response = oauth.get(url, params=params)
        return response
    except:
        if hasattr(e, 'message'):
            logging.error("Error on %s: %s" % (str(url), str(e.message)))
        else:
            logging.error("Error on %s: %s" % (str(url), str(e)))
            sys.exit(1)
def get_users(api, oauth):
    url = str(api).rstrip("/") + "/users"
    params = {}
    response = oauth.get(url, params=params)
    return response
def get_user_videos(api, oauth, user):
    url = str(api).rstrip("/") + "/accounts/" + user + "/videos"
    params = {}
    response = oauth.get(url, params=params)
    return response
def get_user_channels(api, oauth, user):
    url = str(api).rstrip("/") + "/accounts/" + user + "/video-channels"
    params = {}
    response = oauth.get(url, params=params)
    return response
def get_channel_videos(api, oauth, channel):
    url = str(api).rstrip("/") + "/video-channels/" + channel + "/videos"
    params = {}
    response = oauth.get(url, params=params)
    return response
def get_channel_id(api, oauth, channel_id):
    url = str(api).rstrip("/") + "/video-channels/" + str(channel_id)
    params = {}
    response = oauth.get(url, params=params)
    return response.json()['id']

def push_video_api(session, api, oauth, video):
    
    def get_file(path):
        mimetypes.init()
        return (os.path.basename(path), open(os.path.abspath(path), 'rb'), mimetypes.types_map[os.path.splitext(path)[1]])
    #get_authenticated_service(peertube_url, client_id, client_secret)
    url = str(api).rstrip("/") + "/videos/upload"
    fields = {
        'name':             str(video['name']),
        'videofile':        get_file(video['path'])
        }
    if 'channelId' in video:
        fields.update(channelId=str(get_channel_id(api, oauth, video['channelId'])))
    else:
        print('I need a channel id to import video')
        sys.exit(1)
    if 'thumbnail' in video:
        if os.path.isfile(video['thumbnail']):
            fields.update(thumbnailfile=get_file(video['thumbnail']))
            fields.update(previewfile=get_file(video['thumbnail']))
    if 'privacy' in video:
        fields.update(privacy=video['privacy'])
    else:
        fields.update(privacy='1')
    #if 'licence' in video:
    #    fields.update(license=video['license'])
    if 'description' in video:
        fields.update(description=video['description'])
    if 'nsfw' in video:
        if type(video['nsfw']) == int:
            if video['nsfw'] >= 1:
                fields.update(nsfw="1")
            else:
                fields.update(nsfw="0")
        elif type(video['nsfw']) == bool:
            if video['nsfw']:
                fields.update(nsfw="1")
            else:
                fields.update(nsfw="0")
        elif type(video['nsfw']) == str:
            if video['nsfw'].lower() == "true":
                fields.update(nsfw='1')
            else:
                fields.update(nsfw="0")
        else:
            fields.update(nsfw="0")
    
    if 'category' in video:
        ### CATEGORIES ###
        YOUTUBE_CATEGORY = {
            "music": 10,
            "films": 1,
            "vehicles": 2,
            "sport": 17,
            "travels": 19,
            "gaming": 20,
            "people": 22,
            "comedy": 23,
            "entertainment": 24,
            "news & politics": 25,
            "how to": 26,
            "education": 27,
            "activism": 29,
            "science & technology": 28,
            "science": 28,
            "technology": 28,
            "animals": 15
        }
        PEERTUBE_CATEGORY = {
            "music": 1,
            "films": 2,
            "vehicles": 3,
            "sport": 5,
            "travels": 6,
            "gaming": 7,
            "people": 8,
            "comedy": 9,
            "entertainment": 10,
            "news & politics": 11,
            "how to": 12,
            "education": 13,
            "activism": 14,
            "science & technology": 15,
            "science": 15,
            "technology": 15,
            "animals": 16
        }
        def getCategory(category, platform):
            if platform == "youtube":
                return YOUTUBE_CATEGORY[category.lower()]
            else:
                return PEERTUBE_CATEGORY[category.lower()]
        print(str(getCategory(video['category'], 'peertube')))
        fields.update(category=str(getCategory(video['category'], 'peertube')))
    if 'language' in video:
        ### LANGUAGES ###
        YOUTUBE_LANGUAGE = {
            "arabic": 'ar',
            "english": 'en',
            "french": 'fr',
            "german": 'de',
            "hindi": 'hi',
            "italian": 'it',
            "japanese": 'ja',
            "korean": 'ko',
            "mandarin": 'zh-CN',
            "portuguese": 'pt-PT',
            "punjabi": 'pa',
            "russian": 'ru',
            "spanish": 'es'
        }
        PEERTUBE_LANGUAGE = {
            "arabic": "ar",
            "english": "en",
            "french": "fr",
            "german": "de",
            "hindi": "hi",
            "italian": "it",
            "japanese": "ja",
            "korean": "ko",
            "mandarin": "zh",
            "portuguese": "pt",
            "punjabi": "pa",
            "russian": "ru",
            "spanish": "es"
        }
        def getLanguage(language, platform):
            if platform == 'youtube':
                return YOUTUBE_LANGUAGE[language.lower()]
            else:
                return PEERTUBE_LANGUAGE[language.lower()]
        fields.update(language=getLanguage(video['language'], 'peertube'))
    if 'commentsEnabled' in video:
        comment = ""
        if type(video['commentsEnabled']) is str:
            if video['commentsEnabled'] in ["1", "0"]:
                comment = video['commentsEnabled']
            elif video['commentsEnabled'].lower() in ['true', 'false']:
                if video['commentsEnabled'].lower() == 'true':
                    comment = "1"
                else:
                    comment = "0"
        elif type(video['commentsEnabled']) is bool:
            if video['commentsEnabled']:
                comment = "1"
            else:
                comment = "0"
        else:
            comment = "1"
        fields.update(commentsEnabled=comment)
    #if 'tags' in video:
    #    tag_lst = []
    #    if type(video['tags']) is list:
    #        if len(video['tags']) >= 5 :
    #            for i, item in enumerate(video['tags']):
    #                if i >= 5:
    #                    continue
    #                elif len(item) >= 30:
    #                    logging.warning("Peertube does not allow tag with more tha 30 characters")
    #                    continue
    #                else:
    #                    tag_lst.append(item)
    #        else:
    #            tag_lst = list(video['tags'])
    #        fields.update(tags=tag_lst)
    #    else:
    #        tags = video['tags'].split(',')
    #        for i, strtag in enumerate(tags):
    #            if i >= 5:
    #                continue
    #            if strtag == "":
    #                continue
    #            if len(strtag) >= 30:
    #                logging.warning("Peertube does not allow tag with more tha 30 characters")
    #                continue
    #            tag_lst.append(strtag)
    #        fields.update(tags=tag_lst)
    multipart_data = MultipartEncoder(fields)
    print(fields)
    headers = { 'Content-Type': multipart_data.content_type }
    #'Content-Type': multipart_data.content_type }
    logging.info("Uploading video %s" % video['name'])
    response = oauth.post(url, data=multipart_data, headers=headers)
    if response is not None:
        if response.status_code == 200:
            jresponse = response.json()
            jresponse = jresponse['video']
            uuid = jresponse['uuid']
            idvideo = str(jresponse['id'])
            logging.info('Video was successfully uploaded.')
            template = 'Watch it at %s/videos/watch/%s.'
            logging.info(template % (str('https://tube.tr4sk.me'), uuid))
            retval = {  'uuid': uuid,
                        'idvideo': idvideo }
            return retval
        else:
            logging.error(( 'Upload failed with an unexpected response: '
                            '%s') % response)
            logging.error('Reason: %s' % response.reason)
            response.close()
            sys.exit(1)

def check_video_remote(server, path):
    if ssh_key:
        ssh_key_identity = '{}'.format(pipes.quote(ssh_key))
        status = subprocess.call(['ssh', "-i", ssh_key_identity, server, 'test -f {}'.format(pipes.quote(path))])
    else:
        status = subprocess.call(['ssh', server, 'test -f {}'.format(pipes.quote(path))])
    if status == 0:
        return True
    if status == 1:
        return False
    raise Exception('Check remote video failed, could not list video')

def check_video_codec(video):
    for stream in video.streams:
        if stream.is_video():
            return stream.codec()
def check_video_fps(video):
    print(video)
    for stream in video.streams:
        if stream.is_video():
            fps = round(int(stream.frames()) / round(int(stream.duration_seconds())))
            return fps
def check_video_pixel_format(video):
    for stream in video.streams:
        if stream.is_video():
            return stream.pixel_format()
def check_video_size(video):
    for stream in video.streams:
        if stream.is_video():
            return stream.frame_size()

def get_base_bitrate(resolution):
    return {
            '240': 250 * 1000,
            '360': 500 * 1000,
            '480': 900 * 1000,
            '720': 1750 * 1000,
            '1080': 3300 * 1000
            }.get(resolution, None)

def get_target_bitrate(resolution, fps):
    baseBitrate = get_base_bitrate(resolution)
    if not baseBitrate:
        if int(resolution) <= 240:
            baseBitrate = get_base_bitrate('240')
        elif int(resolution) <= 360:
            baseBitrate = get_base_bitrate('360')
        elif int(resolution) <= 480:
            baseBitrate = get_base_bitrate('480')
        elif int(resolution) <= 720:
            baseBitrate = get_base_bitrate('720')
        else:
            baseBitrate = get_base_bitrate('1080')
    maxBitrate = baseBitrate * 1.4
    maxBitrateDifference = int(maxBitrate) - int(baseBitrate)
    maxFpsDifference = int(transcode_fps_max) - int(transcode_fps_average)
    return int(baseBitrate) + (int(fps) - int(transcode_fps_average)) * (int(maxBitrateDifference) / int(maxFpsDifference))
def get_max_bitrate(resolution, fps):
    value = get_target_bitrate(int(resolution), int(fps)) * 2
    return value

def check_audio_codec(video):
    for stream in video.streams:
        if stream.is_audio():
            return stream.codec_name
def check_audio_bitrate(video):
    for stream in video.streams:
        if stream.is_audio():
            return stream.bit_rate

def toBits(kbits):
    return kbits * 8000

def get_transcode(path, uuid):
    best_transcode = ""
    todo_transcode = []
    for transcode in transcode_format:
        #print(video['name'])
        video_file = uuid + '-' + transcode + '.mp4'
        path = remote_path + video_file
        remote_video_status = check_video_remote(server, path)
        if remote_video_status:
            logging.info(path + ': OK')
            if not best_transcode:
                best_transcode = transcode
        else:
            if best_transcode:
                todo_transcode.append(transcode)
            else:
                continue
    retval = {"best_transcode" : best_transcode, "todo_transcode": todo_transcode}
    return retval

def create_job_import_video_file(video_uuid, video_dst):
    command_ssh = ['ssh']
    if ssh_key:
        command_ssh.append('-i')
        command_ssh.append(" {}".format(pipes.quote(ssh_key)))
    command_ssh.append(server)
    create_import_video_file_job = "NODE_CONFIG_DIR=" + format(pipes.quote('/var/www/peertube/config')) + " NODE_ENV=production npm run create-import-video-file-job -- "
    create_import_video_file = "-v " + video_uuid + " -i " + format(pipes.quote(video_dst))
    create_import_video = " \"cd peertube-latest && " + create_import_video_file_job + create_import_video_file + "\""
    command_ssh.append(create_import_video)
    print(command_ssh)
    print(video_dst)
    print(video_uuid)
    if verbose:
      print(command_ssh)
    status = subprocess.call(' '.join(command_ssh), shell=True)
    if status == 0:
        return True
    else:
        return False

def get_video_info(video):
    video_info = ffprobe3.FFProbe(str(video['path']))
    video.update(size           =   check_video_size(video_info))
    video.update(fps            =   check_video_fps(video_info))
    video.update(audio_codec    =   check_audio_codec(video_info))
    video.update(audio_bitrate  =   int(check_audio_bitrate(video_info)))
    return video

def get_transcode_fps(fps, res):
    if int(fps) > transcode_fps_max:
        transcode_fps = transcode_fps_max
    elif int(fps) < transcode_fps_min:
        transcode_fps = transcode_fps_min
    else:
        transcode_fps = int(fps)
    if res in ['480', '360', '240'] and fps > transcode_fps_average:
        transcode_fps = transcode_fps_average
    return transcode_fps
def transcode_to_res(video):
    video = get_video_info(video)
    #video_src_ffprobe = ffprobe3.FFProbe(video_src)
    #video_src_size = check_video_size(video_src_ffprobe)
    #video_src_fps = check_video_fps(video_src_ffprobe)
    #video_src_audio_codec = check_audio_codec(video_src_ffprobe)
    #video_src_audio_bitrate = int(check_audio_bitrate(video_src_ffprobe))
    #transcode_fps = get_transcode_fps(video_src_fps, video_src_size[1])
    #maxrate = get_target_bitrate(dst_res, transcode_fps)
    #bufsize = maxrate * 2
    #video.update(size=check_video_size(video_ffprobe))
    #video.update(fps=check_video_fps(video_ffprobe))
    #video.update(audio_codec=check_audio_codec(video_ffprobe))
    #video.update(audio_bitrate=int(check_audio_bitrate))
    print(video)
    if 'transcode' in video:
        video.update(size=video['transcode'])
    else:
        video.update(size=video['size'][1])
    video.update(target_fps=get_transcode_fps(video['fps'], video['size']))
    video.update(buffsize=int(get_max_bitrate(video['size'], video['fps'])))

    if 'transcode' in video:
        video.update(maxrate=int(get_target_bitrate(video['transcode'], video['target_fps'])))
        transcode_scale = " -filter:v 'scale=w=trunc(oh*a/2)*2:h=%s'" % (video['transcode'])
    else:
        video.update(maxrate=int(get_target_bitrate(video['size'], video['target_fps'])))
        transcode_scale = ''
    
    transcode_rate = "-maxrate %s" % (video['maxrate'])
    transcode_bufsize = "-bufsize %s" % (video['buffsize'])
    transcode_codec = "-vcodec libx264"

    video.update(path_src=video['path'])
    transcode_video_src = video['path']
    transcode_keyframe_interval = "-g %s" % (video['target_fps'] * 2)
    transcode_audio_codec = "libfdk_aac"
    transcode_audio_bitrate = ""
    if video['audio_codec'] == "acc":
        if video['audio_bitrate'] > toBits(transcode_audio_baseKbitrate):
            transcode_audio_bitrate = transcode_audio_baseKbitrate
    elif video['audio_codec'] == "mp3":
        if video['audio_bitrate'] <= toBits(192):
            transcode_audio_bitrate = 128
        elif video['audio_bitrate'] <= toBits(384):
            transcode_audio_bitrate = 256
        else:
            transcode_audio_bitrate = transcode_audio_baseKbitrate
    if transcode_audio_bitrate:
        transcode_audio = "-c:a %s -b:a %sk" % (str(transcode_audio_codec), str(transcode_audio_bitrate))
    else:
        transcode_audio = ""
    transcode_params = " -i " + format(pipes.quote(transcode_video_src)) + " -y " + str(transcode_audio) + " " + str(transcode_codec) + " -r " + str(video['target_fps']) + str(transcode_scale) + " -f mp4 -level 3.1 -b_strategy 1 -bf 16 -pix_fmt yuv420p -map_metadata -1 -movflags faststart " + str(transcode_rate) + " " + str(transcode_bufsize) + " " + str(transcode_keyframe_interval) + " " + format(pipes.quote(video['path_dst']))
    logging.debug("ffmpeg" + transcode_params)
    if not os.path.isfile(video['path_dst']):
        ffmpeg = subprocess.call('ffmpeg' + transcode_params, shell=True)
        if not ffmpeg == 0:
            print("transcode failed for " + video['path_dst'])
            os.remove(video['path_dst'])
            return False
    return video

if __name__ == "__main__":
    args = args()
    action = ""
    api = ""
    login = ""
    password = ""
    server = ""
    user_id = ""
    channel_id = ""
    ssh_key = ""
    verbose = ""
    video_file = ""
    video_name = ""
    thumbnail = ""
    do_not_transcode = ""
    url = ""
    if args.config_file:
        config = configparser.ConfigParser()
        config.read(args.config_file)
        if config.has_option('DEFAULT', 'action'):
            action = config.get('DEFAULT', 'action')
        if config.has_option('DEFAULT', 'server'):
            server = config.get('DEFAULT', 'server')
        if config.has_option('DEFAULT', 'login'):
            login = config.get('DEFAULT', 'login')
        if config.has_option('DEFAULT', 'password'):
            password = str(config.get('DEFAULT', 'password'))
        if config.has_option('DEFAULT', 'api'):
            api = config.get('DEFAULT', 'api')
        if config.has_option('DEFAULT', 'user_id'):
            user_id = config.get('DEFAULT', 'user_id')
        if config.has_option('DEFAULT', 'channel_id'):
            user_id = config.get('DEFAULT', 'channel_id')
        if config.has_option('DEFAULT', 'ssh_key'):
            ssh_key = config.get('DEFAULT', 'ssh_key')
    if args.api:
        api = args.api
    if args.login:
        login = args.login
    if args.password:
        password = args.password
    if args.server:
        server = args.server
    if args.user_id:
        user_id = args.user_id
    if args.channel_id:
        channel_id = args.channel_id
    if args.ssh_key:
        ssh_key = args.ssh_key
    if args.video_file:
        video_file = args.video_file
    if args.video_name:
        video_name = args.video_name
    if args.thumbnail:
        thumbnail = args.thumbnail
    if args.do_not_transcode:
        do_not_transcode = args.do_not_transcode
    if args.url:
        url = args.url
    if args.verbose:
        verbose = True
    if args.action and not action:
        action = args.action
    if not api:
        api = "v1"
    elif not login:
        print("login is missing, please specify one")
        sys.exit(1)
    elif not password:
        print("password is missing, please specify one")
        sys.exit(1)
    elif not server:
        print("server is missing, please specify one")
        sys.exit(1)
    session = requests.session()
    server = server.strip('"').strip("'")
    api = api.strip('"').strip("'")
    login = login.strip("'").strip('"')
    password = password.strip("'").strip('"')
    base_url = 'https://%s' % str(server)
    api_url = '%s/api/%s/' % (str(base_url),str(api))
    server = "peertube@" + server
    oauth = get_oauth_token(session, api_url, login, password)
    if action == "import":
        """
        Create import job. First transcode video source in the proper format then upload it to the server.
        Transcode will then be done for lower resolution and added to the server.
        """
        if video_file:
            video = {}
            video.update(path=str(video_file))
            video = get_video_info(video)
            video_src_dirname = os.path.dirname(video['path'])
            video_src_filename = os.path.basename(video['path'])
            video_src_suffix = os.path.splitext(video_src_filename)
            video_src_prefix = video_src_suffix[0]
            video_src_suffix = video_src_suffix[-1]
            video.update(path_dst=video_src_dirname + '/' + video_src_prefix + '-orig.mp4')
            if not do_not_transcode:
                video = transcode_to_res(video)
            if video_name:
                video.update(name=video_name)
            else:
                video.update(name=video_src_filename)
            if channel_id:
                video.update(channelId=channel_id)
            else:
                video.update(channelId=login + '_channel')
            if thumbnail:
                video.update(thumbnail=thumbnail)
            elif os.path.exists(video_src_dirname + '/' + video_src_prefix + '.jpg'):
                video.update(thumbnail=video_src_dirname + '/' + video_src_prefix + '.jpg')
        elif url:
            def download_video(youtube_option, url, path, video, info):
                video.update(name=info['title'])
                video.update(license=info['license'])
                video.update(description=info['description'])
                video.update(tags=info['tags'])
                video.update(category=str(info['categories'][0]))
                video.update(commentsEnabled=str('true'))
                video.update(nsfw=str('false'))
                if channel_id:
                    video.update(channelId=channel_id)
                else:
                    video.update(channelId=login + '_channel')
                
                dl_opts = dict(youtube_option)
                dl_opts.update(outtmpl='%(uploader_id)s/%(id)s.%(ext)s')
                dl_opts.update(writethumbnail=path)
                dl_opts.update(writesubtitles=path)
                base_path = [path.rstrip('/'), info['uploader_id']]
                video_path = list(base_path)
                video_path.append(info['id'] + '.mp4')
                thumbnail_path = list(base_path)
                thumbnail_path.append(info['id'] + '.jpg')
                video.update(path='/'.join(video_path))
                video.update(thumbnail='/'.join(thumbnail_path))
                ydl_download = youtube_dl.YoutubeDL(dl_opts)
                ydl_file = ydl_download.download([url])
                if not ydl_file == 0:
                    logging.error("Download failed for: " + str(video['name']))
                    sys.exit(1)
                video = get_video_info(video)
                return video

            def progress_hook (d):
                if d['status'] == 'finished':
                    logging.info("Done downloading, now converting…")
            current_dir = os.path.abspath(os.path.curdir)
            temp_dir = str(current_dir) + '/tmp/'
            
            ydl_url = str(url)
            if not os.path.isdir(temp_dir):
                os.mkdir(temp_dir)
            os.chdir(temp_dir)

            ydl_opts = {
                'progress_hooks': [progress_hook],
                #'postprocessors': [{
                #    'key': 'FFmpegVideoConvertor',
                #    'preferedformat': 'mp4'
                #}],
                'merge_output_format': 'mp4',
                'ignoreerrors': True,
                'format': 'bestvideo+bestaudio',
                'quiet': True,
                'no_warnings': True
                }
            video = {}

            ydl = youtube_dl.YoutubeDL(ydl_opts)
            info = ydl.extract_info(ydl_url, download=False)
            if info['extractor_key'] == 'Youtube':
                logging.info("Working on %s (url: %s" % (info['title'], info['webpage_url']))
                r_video = download_video(ydl_opts, ydl_url, temp_dir, video, info)
                if not 'uuid' in video:
                    video_api = push_video_api(session, api_url, oauth, video)
                    video.update(uuid=video_api['uuid'])

            elif info['extractor_key'] == 'YoutubePlaylist':
                for i in info['entries']:
                    logging.info("Playlist detected. Working on %s (url: %s)" % (i['title'], i['webpage_url']))
                    r_video = download_video(ydl_opts, i['webpage_url'], temp_dir, video, i)
                    video_api = push_video_api(session, api_url, oauth, video)
                    video.update(uuid=video_api['uuid'])
            else:
                logging.info("Not supported for now")
                sys.exit(1)
            #for ydl_format in info['formats']:
            #    if str(ydl_format['ext']) == 'mp4' and not str(ydl_format['acodec']) == 'none':
            #        print(ydl_format)
            #        if str(ydl_format['height']) in transcode_format:
            #            dl_opts = dict(ydl_opts)
            #            dl_opts.update(format=ydl_format['format_id'])
            #            dl_opts.update(outtmpl='%(uploader_id)s/%(id)s/' + str(ydl_format['format_note']).rstrip('p') + '.%(ext)s')
            #            ydl_download = youtube_dl.YoutubeDL(dl_opts)
            #            ydl_file = ydl_download.download([ydl_url])
            #            if not ydl_file == 0:
            #                logging.error("Download failed for: " + str(video['name']))
            #                sys.exit(0)
            #            video_path = [temp_dir.rstrip('/'), info['uploader_id'], info['id'], str(ydl_format['format_note']).rstrip('p') + '.' + ydl_format['ext']]
            #            print('/'.join(video_path))
            #            video.update(path='/'.join(video_path))
            #            if not 'uuid' in video:
            #                video_api = push_video_api(session, api_url, oauth, video)
            #                video.update(uuid=video_api['uuid'])
            #            #else:
            #            #    video_upload = create_job_import_video_file(video['uuid'], video['dst'])
            #ydl.download([ydl_url])
            os.chdir(current_dir)
            
        push_video = push_video_api(session, api_url, oauth, video)
        sys.exit(0)
    else:
        if action == "videos":
            videos = get_videos(api_url, oauth)
        elif action == "user":
            if user_id:
                videos = get_user_videos(api_url, oauth, user_id)
            else:
                print("I need an userid (username) to work with")
                sys.exit(1)
        elif action == "channel":
            if channel_id:
                videos = get_channel_videos(api_url, oauth, channel_id)
            else:
                print("I need a channel_id to work with")
                sys.exit(1)
        if verbose:
            print(videos.json()['data'])
        # https://tube.tr4sk.me/download/videos/a610910f-853f-4fab-9940-e7d8de5cd4fb-1080.mp4
        for video in videos.json()['data']:
            video_transcode = get_transcode(remote_path, video['uuid'])
            video.update(transcode_todo=video_transcode['todo_transcode'])
            video.update(transcode_best=video_transcode['best_transcode'])
            for transcode in video['transcode_todo']:
                video.update(transcode=transcode)
                video.update(path=local_path + video['uuid'] + '-' + video['transcode_best'] + '.mp4')
                video.update(path_dst=local_path + video['uuid'] + '-' + transcode + ".mp4")
                rsync_base = ['rsync -az']
                if ssh_key:
                    rsync_base.append("-e '/usr/bin/ssh -i  %s'" % format(pipes.quote(ssh_key)))
                rsync_path_src = server + ':' + remote_path + video['uuid'] + '-' + video['transcode_best'] + '.mp4'
                rsync_path_dst = format(pipes.quote(video['path']))
                rsync_arguments = [rsync_path_src, rsync_path_dst]
                command = list(rsync_base)
                for argument in rsync_arguments:
                    command.append(argument)
                if verbose:
                    print(command)
                command_status = subprocess.call(' '.join(command), shell=True)
                transcode_status = transcode_to_res(video)
                if not transcode_status:
                    break
                rsync_path_src = format(pipes.quote(transcode_status['path_dst'])) + ' '
                rsync_path_dst = server + ':' + format(pipes.quote('/tmp/'))
                rsync_arguments = [rsync_path_src, rsync_path_dst]
                command = list(rsync_base)
                for argument in rsync_arguments:
                    command.append(argument)
                if verbose:
                    print(command)
                #rsync = subprocess.call("rsync -avz " + rsync_path_src + rsync_path_dst, shell=True)
                command_status = subprocess.call(' '.join(command), shell=True)
                if not command_status == 0:
                    print("upload rsync failed")
                    break
                video_dst_job = "%s/%s" % (format(pipes.quote('/tmp')), format(pipes.quote(video['uuid'] + '-' + video['transcode'] + ".mp4")))
                print("Creating job for file: %s" % video_dst_job)
                create_job = create_job_import_video_file(video['uuid'], video_dst_job)
                if not create_job:
                    print("Node import video job failed")
                    break
